﻿using WPF_Template.Interfaces.Base;

namespace WPF_Template.Models.Base
{
    public abstract class BaseDisplayModel<T> : BaseModel<T>, IDisplay
    {
        public string DisplayName { get; }

        protected BaseDisplayModel(string displayName, string defaultDisplayName, bool lockGUID = false) : base(lockGUID)
        {
            DisplayName = string.IsNullOrWhiteSpace(displayName) ? defaultDisplayName : displayName;
        }
    }
}

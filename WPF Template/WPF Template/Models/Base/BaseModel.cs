﻿using System;
using WPF_Template.Interfaces.Base;

namespace WPF_Template.Models.Base
{
    public abstract class BaseModel<T> : IModel
    {
        private static readonly Guid LOCK_GUID = Guid.NewGuid();

        public Guid GUID { get; }

        protected BaseModel(bool lockGUID = false)
        {
            GUID = lockGUID ? LOCK_GUID : Guid.NewGuid();
        }
    }
}

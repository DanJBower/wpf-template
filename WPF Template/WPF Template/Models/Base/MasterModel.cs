﻿using System.Collections.ObjectModel;
using WPF_Template.Interfaces.Base;

namespace WPF_Template.Models.Base
{
    public class MasterModel : BaseModel<MasterModel>, IMaster
    {
        public IDisplayViewModel CurrentDisplayViewModel { get; set; }
        public ObservableCollection<IDisplayViewModel> DisplayViewModels { get; set; }
    }
}

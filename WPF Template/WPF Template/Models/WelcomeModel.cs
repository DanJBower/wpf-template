﻿using WPF_Template.Interfaces;
using WPF_Template.Models.Base;
using WPF_Template.Resources;

namespace WPF_Template.Models
{
    public class WelcomeModel : BaseDisplayModel<WelcomeModel>, IWelcome
    {
        public bool SampleValueChecked { get; set; } = true;

        public WelcomeModel() : base(WelcomeResources.DISPLAY_NAME, WelcomeResources.DISPLAY_NAME, true) { }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel;

namespace WPF_Template.Utility
{
    public static class UtilityMethods
    {
        /// <summary>
        /// This method validates the property name is valid before trying to raise the property change
        /// </summary>
        /// <param name="sender">Origin of call - Normal 'this'</param>
        /// <param name="propertyName">The property name to check</param>
        /// <returns>Returns a boolean. True if it is a valid property name. False if it is not a valid property name</returns>
        public static bool IsValidPropertyName(object sender, string propertyName)
        {
            return TypeDescriptor.GetProperties(sender)[propertyName] != null;
        }

        /// <summary>
        /// This method compares two values to see if they are equal.
        /// </summary>
        /// <typeparam name="T">The type of variable being compared</typeparam>
        /// <param name="x">X is compared to Y</param>
        /// <param name="y">Y is compared to X</param>
        /// <returns>Returns a boolean. <para/>
        /// If it is true, the two values are equal. <para/>
        /// If it is false the two values are not equal</returns>
        /// <example>
        /// <code>
        /// int x = 1;
        /// int y = 2;
        /// Compare(x, y);
        /// </code>
        /// </example>
        public static bool IsEqual<T>(T x, T y)
        {
            return EqualityComparer<T>.Default.Equals(x, y);
        }
    }
}

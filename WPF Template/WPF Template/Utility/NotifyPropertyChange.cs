﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WPF_Template.Utility
{
    public abstract class NotifyPropertyChange : INotifyPropertyChanged
    {
        #region Property Change
        //Required for raising a property change and implemented from INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// This method lets the system know that a properties value has been changed.
        /// </summary>
        /// <param name="propertyName">The name of the property being updated <para/>
        /// If not specified, the CallerMemberName will be used</param>
        /// <returns>Returns a boolean. True if successfully raised a property change. Returns false if it is not a valid property name or if PropertyChanged / the sender object is null</returns>
        private bool TryRaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged == null || !UtilityMethods.IsValidPropertyName(this, propertyName))
            {
                return false;
            }

            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            return true;
        }
        #endregion

        #region Update Methods
        #region Update Fields
        //Method 1
        /// <summary>
        /// Updates a property that uses an accessible field to store the property's value.
        /// </summary>
        /// <typeparam name="T">The type of property being updated</typeparam>
        /// <param name="field">Reference to the field that stores the property's value</param>
        /// <param name="newValue">The property's new value</param>
        /// <param name="performEqualityCheck">Defines whether the equality check is done</param>
        /// <param name="propertyName">The property being updated. This is the CallerMemberName if it is not explicitly defined</param>
        /// <returns>A boolean. If the values are equal, the boolean is false as the property has not changed. Otherwise it returns true</returns>
        /// <example>
        /// <code>
        /// private string field;
        /// public string Field
        /// {
        ///     get => field;
        ///     set => TrySetField(ref field, value);
        /// }
        /// </code>
        /// </example>
        protected bool TrySetField<T>(ref T field, T newValue, bool performEqualityCheck = true, [CallerMemberName] string propertyName = null)
        {
            if (performEqualityCheck && UtilityMethods.IsEqual(field, newValue))
            {
                return false;
            }

            field = newValue;

            if (!TryRaisePropertyChanged(propertyName))
            {
                return false;
            }

            return true;
        }
        #endregion

        #region Update Properties
        //Method 2 - Action - Fastest TrySetProperty method
        /// <summary>
        /// Updates a property that uses an accessible property to store the property's value.
        /// </summary>
        /// <typeparam name="T">The type of property being updated</typeparam>
        /// <param name="property">An action used for setting the property. See example of how to write</param>
        /// <param name="newValue">The property's new value</param>
        /// <param name="oldValue">The property's old value for equality check</param>
        /// <param name="performEqualityCheck">Defines whether the equality check is done</param>
        /// <param name="propertyName">The property being updated. This is the CallerMemberName if it is not explicitly defined</param>
        /// <returns>A boolean. If the values are equal, the boolean is false as the property has not changed. Otherwise it returns true</returns>
        /// <example>
        /// <code>
        /// public int Prop {
        /// 	get => Model.Prop;
        /// 	set => TrySetProperty(x => Model.Prop = x, value, Model.Prop);
        /// }
        /// </code>
        /// </example>
        protected bool TrySetProperty<T>(Action<T> property, T newValue, T oldValue, bool performEqualityCheck = true, [CallerMemberName] string propertyName = null)
        {
            if (performEqualityCheck && UtilityMethods.IsEqual(oldValue, newValue))
            {
                return false;
            }

            property(newValue);

            if (!TryRaisePropertyChanged(propertyName))
            {
                return false;
            }

            return true;
        }
        #endregion
        #endregion
    }
}

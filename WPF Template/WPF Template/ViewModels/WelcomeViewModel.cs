﻿using WPF_Template.Interfaces;
using WPF_Template.Models;
using WPF_Template.ViewModels.Base;

namespace WPF_Template.ViewModels
{
    public class WelcomeViewModel : BaseDisplayViewModel<WelcomeModel>, IWelcome
    {
        public bool SampleValueChecked {
            get => Model.SampleValueChecked;
            set => TrySetProperty(x => Model.SampleValueChecked = x, value, Model.SampleValueChecked);
        }

        public WelcomeViewModel() : base(new WelcomeModel()) { }
    }
}

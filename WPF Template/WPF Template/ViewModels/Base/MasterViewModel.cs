﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using WPF_Template.Interfaces.Base;
using WPF_Template.Models.Base;
using WPF_Template.Utility;

namespace WPF_Template.ViewModels.Base
{
    public sealed class MasterViewModel : BaseViewModel<MasterModel>, IMaster
    {
        public RelayCommand ChangeDisplayViewModel { get; }

        public IDisplayViewModel CurrentDisplayViewModel {
            get => Model.CurrentDisplayViewModel;
            set => TrySetProperty(x => Model.CurrentDisplayViewModel = x, value, Model.CurrentDisplayViewModel);
        }

        public ObservableCollection<IDisplayViewModel> DisplayViewModels {
            get => Model.DisplayViewModels;
            set => TrySetProperty(x => Model.DisplayViewModels = x, value, Model.DisplayViewModels);
        }

        private static readonly Lazy<MasterViewModel> LAZY =
            new Lazy<MasterViewModel>(() => new MasterViewModel());

        public static MasterViewModel Instance => LAZY.Value;

        private MasterViewModel() : base(new MasterModel())
        {
            ChangeDisplayViewModel = new RelayCommand(
                displayViewModel => TryChangeDisplay((IDisplayViewModel)displayViewModel),
                displayViewModel => displayViewModel is IDisplay);
            DisplayViewModels = new ObservableCollection<IDisplayViewModel>();

            IDisplayViewModel initialDisplay = new WelcomeViewModel();
            TryAddDisplay(initialDisplay);

            CurrentDisplayViewModel = initialDisplay;
        }

        public bool TryAddDisplay(IDisplayViewModel displayViewModel)
        {
            if (DisplayViewModels.All(dvm => dvm.GUID != displayViewModel.GUID))
            {
                DisplayViewModels.Add(displayViewModel);
                return true;
            }
            return false;
        }

        public bool TryChangeDisplay(IDisplayViewModel displayViewModel)
        {
            IDisplayViewModel value = DisplayViewModels.FirstOrDefault(
                       dvm => dvm.GUID == displayViewModel.GUID);
            if (value != null)
            {
                CurrentDisplayViewModel = value;
                return true;
            }
            return false;
        }
    }
}

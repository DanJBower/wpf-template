﻿using System;
using WPF_Template.Interfaces.Base;
using WPF_Template.Models.Base;
using WPF_Template.Utility;

namespace WPF_Template.ViewModels.Base
{
    public abstract class BaseViewModel<T> : NotifyPropertyChange, IModel where T : BaseModel<T>
    {
        protected T Model { get; }

        public Guid GUID => Model.GUID;

        protected BaseViewModel(T model)
        {
            Model = model;
        }
    }
}

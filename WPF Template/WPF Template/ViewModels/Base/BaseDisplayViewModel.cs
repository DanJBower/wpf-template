﻿using WPF_Template.Interfaces.Base;
using WPF_Template.Models.Base;

namespace WPF_Template.ViewModels.Base
{
    public abstract class BaseDisplayViewModel<TModel> : BaseViewModel<TModel>, IDisplayViewModel where TModel : BaseDisplayModel<TModel>
    {
        public string DisplayName => Model.DisplayName;

        protected BaseDisplayViewModel(TModel model) : base(model) { }
    }
}

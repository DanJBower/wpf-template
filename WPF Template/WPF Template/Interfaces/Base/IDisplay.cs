﻿namespace WPF_Template.Interfaces.Base
{
    public interface IDisplay : IModel
    {
        string DisplayName { get; }
    }
}

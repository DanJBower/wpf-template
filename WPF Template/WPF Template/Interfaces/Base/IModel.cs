﻿using System;

namespace WPF_Template.Interfaces.Base
{
    public interface IModel
    {
        Guid GUID { get; }
    }
}

﻿using System.Collections.ObjectModel;

namespace WPF_Template.Interfaces.Base
{
    public interface IMaster : IModel
    {
        IDisplayViewModel CurrentDisplayViewModel { get; set; }
        ObservableCollection<IDisplayViewModel> DisplayViewModels { get; set; }
    }
}

﻿using WPF_Template.Interfaces.Base;

namespace WPF_Template.Interfaces
{
    public interface IWelcome : IDisplay
    {
        bool SampleValueChecked { get; set; }
    }
}

﻿using WPF_Template.ViewModels.Base;

namespace WPF_Template
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = MasterViewModel.Instance;
        }
    }
}
